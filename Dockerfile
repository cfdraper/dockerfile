FROM byuhbll/base

RUN cd /opt && \
  curl -LOJ "https://github.com/AdoptOpenJDK/openjdk8-openj9-releases/releases/download/jdk8u181-b13_openj9-0.9.0/OpenJDK8-OPENJ9_x64_Linux_jdk8u181-b13_openj9-0.9.0.tar.gz" && \ 
  tar xvzf *.tar.gz && \
  rm *.tar.gz

ENV DEFAULT_SERVICES="hang"

EXPOSE 8080

CMD ["start"]

USER root
